#!/bin/bash

foreman-installer \
  --enable-foreman-proxy \
  --foreman-proxy-tftp=true \
  --foreman-proxy-tftp-servername=10.2.0.4 \
  --foreman-proxy-dhcp=true \
  --foreman-proxy-dhcp-interface=ens18 \
  --foreman-proxy-dhcp-gateway=10.2.0.1 \
  --foreman-proxy-dhcp-range="10.2.0.5 10.2.0.254" \
  --foreman-proxy-dhcp-nameservers="10.2.0.4" \
  --foreman-proxy-dns=true \
  --foreman-proxy-dns-interface=ens18 \
  --foreman-proxy-dns-zone=mjhooker.lan \
  --foreman-proxy-dns-reverse=2.10.in-addr.arpa \
  --foreman-proxy-dns-forwarders=10.2.0.1 \
  --foreman-proxy-foreman-base-url=https://foreman.mjhooker.lan \
  --foreman-proxy-oauth-consumer-key=kjY3T6iSEKAjHD8fhtygwZPmWadfacSn \
  --foreman-proxy-oauth-consumer-secret=cn67EsgGuCbXoeu8Tb7LyYsRouNGkCwg \
  --enable-foreman-proxy-plugin-discovery \
  --foreman-proxy-plugin-discovery-install-images=true \
  --enable-foreman-plugin-discovery \
  --enable-foreman-plugin-setup
  